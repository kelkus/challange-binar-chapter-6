const{user_game_histories} = require('../models')

const createActivity = (activityInfo) =>{
    const {player_choice,comp_choice,result,user_game_id} = activityInfo
    
    return user_game_histories.create ({
        user_game_id,player_choice,comp_choice,result 
    })
}

module.exports = {createActivity} 