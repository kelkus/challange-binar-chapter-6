const { response } = require('express')
const express = require('express')
const router = express.Router()

router.use((req,res,next)=>{
    console.log('router level middleware')
    next()
})

router.get('/home', (req,res,) => {
    res.render('index')
})

router.get('/game', (req,res,) => {
    res.render('game')
})


module.exports = router