const { response } = require('express')
const express = require('express')
const { DATE } = require('sequelize')
const router = express.Router()
const users = []
const {createUser, getAllUser} = require('../service/user')

router.use((req,res,next)=>{
    console.log('router level middleware')
    next()
})

router.get('/', (req,res,) => {
    res.render('dashboard')
})

router.get('/game-activity', async(req,res,) => {
    const users = await getAllUser()
    res.render('useractivity',{
        users
    })
})

router.get('/manage-user', (req,res,) => {
    res.render('adduser')
})

router.post('/manage-user', async(req,res) => {
    const user = await createUser(req.body)
    users.push(user)
    res.redirect('/dashboard')
})

module.exports = router