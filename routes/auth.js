const { render } = require('ejs')
const { response } = require('express')
const express = require('express')
const { DATE } = require('sequelize')
const router = express.Router()
const users = []
const {user_game,user_game_biodata,user_game_histories} = require('../models')
const {createUser, userLogin} = require('../service/user')


// route level middleware
router.use((req,res,next)=>{
    console.log('router level middleware')
    next()
})

router.get('/login', (req,res,) => {
    res.render('login')
})
router.post('/login', async(req,res,next) => {
   
    const user = await userLogin(req.body)   

    if(!user){
        return res.render('error')
    }

    //LOGIN_time
    // await user_game_histories.create ({
    //     user_game_id: user.id,
    //     login_time: new Date()
    // })

    // const user_data = await user_game.findOne({
    //     where: {
    //         email
    //     }, 
    //     include: [user_game_biodata,user_game_histories]
    // })
    // res.json(user_data)

    users.push(user)
    // console.log(users)
    res.redirect('/')
})



router.get('/registerSucces', (req,res,) => {
    res.render('registerSucces')
})
// router.post('/registerSucces', (req,res,) => {
//     res.redirect('/auth/login')
// })

router.get('/register', (req,res,) => {
    res.render('register')
})

router.post('/register', async(req,res) => {
   
    const user = await createUser(req.body)
    users.push(user)
    // console.log(users)
    res.redirect('/auth/registerSucces')
})

module.exports = router