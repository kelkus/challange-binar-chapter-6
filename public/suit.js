
function getPilihanComputer(){
    const comp = Math.random();
    if( comp < 0.34 ) return 'kertas'; 
    if( comp >= 0.34 && comp < 0.67 ) return 'gunting'; 
    return 'batu'; 
}

function getHasil(comp, player){
    if( player == comp ) return 'seri';
    if( player == 'gunting-user' ) return ( comp == 'kertas' ) ? 'menang' : 'kalah';
    if( player == 'kertas-user' ) return ( comp == 'gunting' ) ? 'kalah' : 'menang';
    if( player == 'batu-user' ) return ( comp == 'gunting' ) ? 'menang' : 'kalah';
    
}

function setHide(){
  document.querySelector('#player-win').setAttribute('class','d-none');
  document.querySelector('#draw').setAttribute('class','d-none');
  document.querySelector('#comp-win').setAttribute('class','d-none');
  document.querySelector('#default').setAttribute('class','d-none');
  document.querySelector('#refresh').setAttribute('class','d-none');
}

function setHasil(hasil){
  if(hasil == 'menang'){
    setHide();
    document.querySelector('#player-win').setAttribute('class','d-block bg-success p-4');
    document.querySelector('#refresh').setAttribute('class','d-block ');
  }
  else if (hasil == 'seri'){
    setHide();
    document.querySelector('#draw').setAttribute('class','d-block bg-success p-5');
    document.querySelector('#refresh').setAttribute('class','d-block ');
  }
  else if (hasil == 'kalah'){
    setHide();
    document.querySelector('#comp-win').setAttribute('class','d-block bg-success p-4');
    document.querySelector('#refresh').setAttribute('class','d-block ');
  }
}

function setComputer(pilihanComputer){
  if (pilihanComputer == 'kertas') {
    document.querySelector('#kertas-c').setAttribute('class','active-com');
  } 
  else if (pilihanComputer == 'gunting') {
    document.querySelector('#gunting-c').setAttribute('class','active-com');
  } 
  else if (pilihanComputer == 'batu') {
    document.querySelector('#batu-c').setAttribute('class','active-com',);
  }
}

function setFreeze(hasil){
  if (hasil != null){
    document.querySelector('#kertas-user').classList.add('no-click');
    document.querySelector('#gunting-user').classList.add('no-click');
    document.querySelector('#batu-user').classList.add('no-click');
  }
}


function refreshButton(){
  hasil = null;
  document.querySelector('#batu-c').setAttribute('class','option-comp');
  document.querySelector('#kertas-c').setAttribute('class','option-comp');
  document.querySelector('#gunting-c').setAttribute('class','option-comp');
  document.querySelector('#player-win').setAttribute('class','d-none');
  document.querySelector('#default').setAttribute('class','d-block p-5');
  document.querySelector('#comp-win').setAttribute('class','d-none');
  document.querySelector('#draw').setAttribute('class','d-none');
  document.querySelector('#kertas-user').classList.remove('no-click');
  document.querySelector('#gunting-user').classList.remove('no-click');
  document.querySelector('#batu-user').classList.remove('no-click');
  document.querySelector('#kertas-user').classList.remove('choosen');
  document.querySelector('#gunting-user').classList.remove('choosen');
  document.querySelector('#batu-user').classList.remove('choosen');
  document.querySelector('#refresh').setAttribute('class','d-block ,pt-5');
}

// const pBatu = document.querySelector('#batu-user');
// const pKertas = document.querySelector('#kertas-user');
// const pGunting = document.querySelector('#gunting-user');

// pBatu.addEventListener('click',function(){
//   this.classList.add("choosen")
//   const pilihanComputer = getPilihanComputer(); 
//   let pilihanPlayer = 'batu';
//   var hasil = getHasil(pilihanComputer, pilihanPlayer);
//   setHasil(hasil);
//   setComputer(pilihanComputer);
//   setFreeze(hasil);

//   console.log("Pilihan Player: "+pilihanPlayer);
//   console.log("Pilihan Computer: "+pilihanComputer);
//   console.log("Hasil: "+hasil);
// })


// pKertas.addEventListener('click',function(){
//   this.classList.add("choosen")
//   const pilihanComputer = getPilihanComputer(); 
//   let pilihanPlayer = 'kertas';
//   const hasil = getHasil(pilihanComputer, pilihanPlayer);
//   setHasil(hasil);
//   setComputer(pilihanComputer);
//   setFreeze(hasil);
  
//   console.log("Pilihan Player: "+pilihanPlayer);
//   console.log("Pilihan Computer: "+pilihanComputer);
//   console.log("Hasil: "+hasil);
// })

// pGunting.addEventListener('click',function(){
//   this.classList.add("choosen")
//   const pilihanComputer = getPilihanComputer(); 
//   let pilihanPlayer = 'gunting';
//   const hasil = getHasil(pilihanComputer, pilihanPlayer);
//   setHasil(hasil);
//   setComputer(pilihanComputer);
//   setFreeze(hasil);

//   console.log("Pilihan Player: "+pilihanPlayer);
//   console.log("Pilihan Computer: "+pilihanComputer);
//   console.log("Hasil: "+hasil);
// })

const playerChoose = document.querySelectorAll('.myoption img')
const user_id = document.getElementById('user_id').getAttribute("value")



playerChoose.forEach(function(pil){
  pil.addEventListener('click',function(){
    this.classList.add("choosen")
    const pilihanComputer = getPilihanComputer(); 
    let pilihanPlayer = pil.id; 
    const hasil = getHasil(pilihanComputer, pilihanPlayer);  

    setHasil(hasil);
    setComputer(pilihanComputer);
    setFreeze(hasil); 
    console.log("Pilihan Player: "+pilihanPlayer);
    console.log("Pilihan Computer: "+pilihanComputer);
    console.log("Hasil: "+hasil);
    console.log("user_id:", String(user_id).split("PLAYER"));
  })
})



const pRefresh = document.querySelector('#refresh');
pRefresh.addEventListener('click',function(){
    refreshButton();
    
})









